-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2019 at 09:48 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mmresto`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id_cart` int(200) NOT NULL,
  `id_order` int(200) NOT NULL,
  `id_masakan` int(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `ket` text NOT NULL,
  `status_cart` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_order`
--

CREATE TABLE `detail_order` (
  `id_detail_order` int(200) NOT NULL,
  `id_order` int(200) NOT NULL,
  `id_masakan` int(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan_detail` text NOT NULL,
  `status_detail_order` varchar(1) NOT NULL DEFAULT 'Y' COMMENT 'Y=Dipesan'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_order`
--

INSERT INTO `detail_order` (`id_detail_order`, `id_order`, `id_masakan`, `jumlah`, `keterangan_detail`, `status_detail_order`) VALUES
(119, 49, 14, 3, '', 'Y'),
(120, 49, 15, 4, 'yg enak', 'Y'),
(121, 49, 13, 5, '', 'Y'),
(122, 49, 15, 3, 'oo', 'Y'),
(123, 49, 12, 2, 'po', 'Y'),
(124, 45, 9, 3, '', 'Y'),
(125, 48, 9, 3, '', 'Y'),
(126, 48, 11, 6, '', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` enum('Administrator','Waitres','Kasir','Owner','Pelanggan') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'Administrator'),
(2, 'Waitres'),
(3, 'Kasir'),
(4, 'Owner'),
(5, 'Pelanggan');

-- --------------------------------------------------------

--
-- Table structure for table `masakan`
--

CREATE TABLE `masakan` (
  `id_masakan` int(100) NOT NULL,
  `nama_masakan` varchar(50) NOT NULL,
  `jenis` enum('Makanan','Minuman','Paketan') NOT NULL,
  `harga` int(200) NOT NULL,
  `desk_masakan` text NOT NULL,
  `gambar` varchar(200) NOT NULL,
  `status_masakan` varchar(1) NOT NULL DEFAULT 'Y' COMMENT 'N Habis, Y Tersedia'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `masakan`
--

INSERT INTO `masakan` (`id_masakan`, `nama_masakan`, `jenis`, `harga`, `desk_masakan`, `gambar`, `status_masakan`) VALUES
(9, 'Paket 1', 'Paketan', 25000, 'Daging Wagiu Medium + 3 Pilihan Saus + Kentang Original', 'food6.jpg', 'Y'),
(10, 'Jus Mangga Tropika', 'Minuman', 10000, 'Dibuat dari Mangga segar yg dipetik dari kebun petani dijamin bikin galau kamu langsung hilang seketika', 'cercle2.png', 'Y'),
(11, 'Paket 2', 'Paketan', 35000, 'Ayam Selimut Panas (5 potong)', 'menu11-thumb.jpg', 'Y'),
(12, 'Kebab Cinta', 'Makanan', 15000, 'Kebab dengan isian daging panggang + sayuran yg cocok banget dimakan berdua sama kamu ', 'menu2-thumb.jpg', 'Y'),
(13, 'Spatiaw', 'Makanan', 20000, 'Kuetiaw yg dimasak ala spageti italia dengan citarasa indonesia bikin kamu berasa kaya keliling dunia', 'menu1-thumb.png', 'Y'),
(14, 'Salad Sayur', 'Makanan', 8000, 'Terdiri dari macam-macam sayur yg pasti bisa bikin kamu sehat dan cocok banget buat kamu yg lagi diet', 'menu10-thumb.jpg', 'Y'),
(15, 'Udang Tomyam', 'Makanan', 23000, 'ga perlu jauh-jauh ke Thailand buat mmakan tomyam, cukup disini aja sama aku kal kejauhan tar kamu kangen', 'menu4-thumb.jpg', 'Y'),
(16, 'Pastik', 'Makanan', 20000, 'pasta dengan bentuk stik ini emang ga janji bikin hubungan kita jadi pastik, *ehh pasti maksudnya', 'OFDM-bg.jpg', 'Y'),
(17, 'Paket 4', 'Paketan', 45000, 'Burger + Kentang Goreng Original + Pepsi + Salad Sayur', 'paketann.jpg', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `meja`
--

CREATE TABLE `meja` (
  `no_meja` int(11) NOT NULL,
  `gambar` varchar(200) NOT NULL,
  `status_meja` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Tersedia, Y Dipesan'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meja`
--

INSERT INTO `meja` (`no_meja`, `gambar`, `status_meja`) VALUES
(1, 'kiteleee.png', 'Y'),
(2, 'kiteleee.png', 'N'),
(3, 'kiteleee.png', 'Y'),
(4, 'kiteleee.png', 'Y'),
(5, 'kiteleee.png', 'N'),
(6, 'kiteleee.png', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `pesan_meja`
--

CREATE TABLE `pesan_meja` (
  `id_order` int(200) NOT NULL,
  `no_meja` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `id_user` int(11) NOT NULL,
  `keterangan` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Belum Memesan, Y Sudah Memesan',
  `status_order` varchar(1) NOT NULL DEFAULT 'Y' COMMENT 'N Belum Dibayar, Y Lunas',
  `token` varchar(20) NOT NULL,
  `nama_pelanggan` varchar(35) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesan_meja`
--

INSERT INTO `pesan_meja` (`id_order`, `no_meja`, `tanggal`, `id_user`, `keterangan`, `status_order`, `token`, `nama_pelanggan`) VALUES
(35, 3, '2019-03-03 20:30:28', 1, 'Y', 'Y', '94838', 'NULL'),
(36, 4, '2019-03-03 20:35:03', 1, 'Y', 'Y', '71086', 'NULL'),
(39, 1, '2019-03-09 05:15:49', 1, 'Y', 'Y', '13088', 'rezki'),
(40, 6, '2019-03-09 16:13:27', 1, 'Y', 'Y', '61885', 'kitelee epop'),
(41, 4, '2019-03-09 16:24:48', 2, 'Y', 'Y', '62886', 'rina'),
(42, 1, '2019-03-09 16:25:05', 1, 'Y', 'Y', '16124', 'NULL'),
(43, 2, '2019-03-09 19:21:17', 1, 'Y', 'Y', '90794', 'kiteleee'),
(44, 1, '2019-03-10 10:05:41', 1, 'N', 'N', '78563', 'kikikiki'),
(45, 2, '2019-03-10 11:12:01', 1, 'Y', 'Y', '54835', 'fupooo'),
(46, 3, '2019-03-10 11:15:59', 2, 'Y', 'N', '57604', 'ruru'),
(47, 4, '2019-03-18 20:35:15', 2, 'Y', 'Y', '32626', 'PAPAPA'),
(48, 4, '2019-04-01 14:06:42', 2, 'Y', 'N', '74552', 'asasvsx'),
(49, 5, '2019-04-02 10:08:09', 2, 'Y', 'Y', '77633', 'rezki');

-- --------------------------------------------------------

--
-- Table structure for table `recovery_keys`
--

CREATE TABLE `recovery_keys` (
  `rid` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `token` varchar(50) NOT NULL,
  `valid` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recovery_keys`
--

INSERT INTO `recovery_keys` (`rid`, `id_user`, `token`, `valid`) VALUES
(1, 1, '23df823af09bda5bd623486ec8cdf924', 1),
(2, 1, 'c334df6729217d4126ee1ce3523bbb85', 1),
(3, 1, '9be5675ef3e45b76c32388f6dce81192', 1),
(4, 1, 'f81945738fab552d664e72dd4e9f7668', 1),
(5, 1, '84b9f6113690c34b72b8a68cc5690f7d', 1),
(6, 1, 'a6a7e9ebb06166d46980016b5debef64', 1),
(7, 2, '3a8025409f1a28a3f4887b9df270e64e', 1),
(8, 1, '851f0e47da89326b720fe0f4d561ea10', 1),
(9, 1, 'b662d0e8caab1c1e55298452f36bbe06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(200) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_order` int(200) NOT NULL,
  `tanggal` datetime NOT NULL,
  `total_bayar` int(200) NOT NULL,
  `yg_dibayarkan` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_user`, `id_order`, `tanggal`, `total_bayar`, `yg_dibayarkan`) VALUES
(25, 1, 42, '2019-03-09 19:17:04', 151000, 160000),
(26, 3, 43, '2019-03-09 19:28:11', 115000, 120000),
(27, 1, 47, '2019-04-01 11:33:48', 75000, 100000),
(28, 1, 49, '2019-04-02 12:03:03', 315000, 2100000),
(29, 1, 45, '2019-04-02 13:04:03', 75000, 80000);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `id_level` int(11) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N tidak aktif , Y Sudah Aktif'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama_user`, `username`, `password`, `email`, `id_level`, `status`) VALUES
(1, 'kiteleee', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'rezkikartikapermana@gmail.com', 1, 'Y'),
(2, 'waiter001', 'waiter', 'f64cff138020a2060a9817272f563b3c', 'daviperdiansyah44@gmail.com', 2, 'Y'),
(3, 'kasir001', 'kasir', 'c7911af3adbd12a035b289556d96470a', 'kasir@gmail.com', 3, 'Y'),
(4, 'owner', 'owner', '72122ce96bfec66e2396d2e25225d70a', 'owner@gmail.com', 4, 'Y'),
(5, 'pelanggan001', 'pelanggan001', '0d61130a6dd5eea85c2c5facfe1c15a7', 'plgn@gmail.com', 5, 'N'),
(16, 'kiteleee', 'kiki', '730a124cb2359aa4e55161685369cede', 'rezkikartikapermana@gmail.com', 2, 'N'),
(17, 'kikipopo', 'kikipopo', '0153ca46dcbee3520eddfea94249e71e', 'kasirpopo123@gmail.com', 5, 'N');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id_cart`);

--
-- Indexes for table `detail_order`
--
ALTER TABLE `detail_order`
  ADD PRIMARY KEY (`id_detail_order`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `masakan`
--
ALTER TABLE `masakan`
  ADD PRIMARY KEY (`id_masakan`);

--
-- Indexes for table `meja`
--
ALTER TABLE `meja`
  ADD PRIMARY KEY (`no_meja`);

--
-- Indexes for table `pesan_meja`
--
ALTER TABLE `pesan_meja`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `recovery_keys`
--
ALTER TABLE `recovery_keys`
  ADD PRIMARY KEY (`rid`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id_cart` int(200) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `detail_order`
--
ALTER TABLE `detail_order`
  MODIFY `id_detail_order` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `masakan`
--
ALTER TABLE `masakan`
  MODIFY `id_masakan` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `pesan_meja`
--
ALTER TABLE `pesan_meja`
  MODIFY `id_order` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `recovery_keys`
--
ALTER TABLE `recovery_keys`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
