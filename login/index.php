﻿<?php
error_reporting(0); 
include"header.php";
include"../koneksi.php";
session_start();

if($_SESSION['id_level']=="1"){
  header("location:../admin/index.php");
}

elseif($_SESSION['id_level']=="2"){
  header("location:../waiter/index.php");
}

elseif($_SESSION['id_level']=="3"){
  header("location:../kasir/index.php");
}

elseif($_SESSION['id_level']=="4"){
  header("location:../owner/index.php");
}

elseif($_SESSION['id_level']=="5"){
  header("location:../pelanggan/index.php");
}
 ?>

        <div class="card">
            <div class="body">
                <form action="proses.php" id="sign_in" method="POST">
                    <div class="msg">Sign in to start your session</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Remember Me</label>
                        </div>
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" type="submit" name="login" id="login">SIGN IN</button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="register.php">Register Now!</a>
                        </div>
                        <div class="col-xs-6 align-right">
                            <a href="forgot.php">Forgot Password?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php
include 'footer.php';
?>