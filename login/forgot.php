<?php
include ('../koneksi.php');
include ('function.php');
	
if(isset($_POST['submit']))
{
	$uemail = $_POST['email'];
	$uemail = mysqli_real_escape_string($conn, $uemail);
	
	if(checkUser($uemail) == "true")
	{
		$id_user = id_user($uemail);
		$token = generateRandomString();
		
		$query = mysqli_query($conn, "INSERT INTO recovery_keys (id_user, token) VALUES ($id_user, '$token') ");
		if($query)
		{
			 $send_mail = send_mail($uemail, $token);


			if($send_mail === 'success')
			{
				 $msg = 'A mail with recovery instruction has sent to your email.';
				 $msgclass = 'bg-success';
			}else{
				$msg = 'There is something wrong.';
				$msgclass = 'bg-danger';
			}

		}else
		{
				$msg = 'There is something wrong.';
				 $msgclass = 'bg-danger';
		}
		
	}else
	{
		$msg = "This email doesn't exist in our database.";
				 $msgclass = 'bg-danger';
	}
}

?>
<?php
include 'header.php';
?>
        <div class="card">
            <div class="body">
                <form id="forgot_password" method="POST" action="">
                    <div class="msg">
                        Masukan alamat email yg digunakan saat mendaftar dan kami akan mengirim token untuk merubah kata sandi..
                    </div>
                    <?php if(isset($msg)) {?>
                    <div class="<?php echo $msgclass; ?>" style="padding:5px;"><?php echo $msg; ?></div>
                <?php } ?>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Email" required autofocus>
                        </div>
                    </div>

                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit" name="submit" >SEND</button>

                    <div class="row m-t-20 m-b--5 align-center">
                        <a href="index.php">Sign In!</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
include 'footer.php';
?>