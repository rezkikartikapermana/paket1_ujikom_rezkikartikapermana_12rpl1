<?php

include ('../koneksi.php');
include ('function.php');

$uemail = $_GET['email'];
$token = $_GET['token'];

$id_user = id_user($uemail); 

$verifytoken = verifytoken($id_user, $token);


if(isset($_POST['submit']))
{
    $new_password = $_POST['password'];
    $new_password = md5($new_password);
    $retype_password = $_POST['password'];
    $retype_password = md5($retype_password);
    
    if($new_password == $retype_password)
    {
        $update_password = mysqli_query($conn, "UPDATE user SET password = '$new_password' WHERE id_user = $id_user");
        if($update_password)
        {
                mysqli_query($conn, "UPDATE recovery_keys SET valid = 0 WHERE id_user = $id_user AND token ='$token'");
                $msg = 'Your password has changed successfully. Please login with your new passowrd.';
                $msgclass = 'bg-success';
        }
    }else
    {
         $msg = "Password doesn't match";
         $msgclass = 'bg-danger';
    }
    
}


?>

<?php
include 'header.php';
?>
        <div class="card">
            <div class="body">
                <?php if($verifytoken == 1) { ?>
                <form id="forgot_password" method="POST">
                    <div class="msg">
                        Masukan kata sandi anda yg baru..
                    </div>
                    <?php if(isset($msg)) {?>
                    <div class="<?php echo $msgclass; ?>" style="padding:5px;"><?php echo $msg; ?></div>
                <?php } ?>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="password" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="confirm" minlength="6" placeholder="Confirm Password" required>
                        </div>
                    </div>

                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">RESET MY PASSWORD</button>

                    <div class="row m-t-20 m-b--5 align-center">
                        <a href="index.php">Sign In!</a>
                    </div>
                </form>
                <?php }else {?>
            <div class="col-lg-4 col-lg-offset-4">
                <h2>Invalid or Broken Token</h2>
                <p>Opps! The link you have come with is maybe broken or already used. Please make sure that you copied the link correctly or request another token from <a href="index.php">here</a>.</p>
            </div>
        <?php }?>
            </div>
        </div>
    </div>
<?php
include 'footer.php';
?>