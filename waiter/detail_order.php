<?php
include "header.php";
?>
<?php
include "../koneksi.php";

$edit = mysqli_query($conn, "SELECT * FROM detail_order INNER JOIN masakan ON detail_order.id_masakan = masakan.id_masakan where id_order='$_GET[id_order]'");
$r=mysqli_fetch_array($edit);
?>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <a href="order.php" class="btn btn-danger">Kembali</a>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                   <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Pesanan</th>
                        <th>Keterangan</th>
                        <th>Jumlah</th>
                        <th>Harga</th>
                        <th>Total</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                      <?php
include "../koneksi.php";
$no=1;
$total=0;
$data = "SELECT * FROM detail_order INNER JOIN masakan ON detail_order.id_masakan = masakan.id_masakan where id_order='$_GET[id_order]' AND status_detail_order='Y'";
$bacadata = mysqli_query($conn, $data);
while($r= mysqli_fetch_array($bacadata))
{
?>
                    <tbody>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $r['nama_masakan'];?></td>
                        <td><?php echo $r['keterangan_detail'];?></td>
                        <td><?php echo $r['jumlah'];?></td>
                        <td>Rp.<?php echo number_format($r['harga']);?></td>
                        <td>Rp.<?php echo number_format($r['jumlah']*$r['harga']);?></td>
                        <td><a href="cancel_orderan.php?id_detail_order=<?php echo $r['id_detail_order'];?>&id_order=<?php echo $r['id_order'];?>" class="btn btn-danger btn-md">
                                <i class="material-icons">delete</i>
                            </a>
                        </td>
                      </tr>
                    </tbody>
                    <?php 
                    $total += ($r['jumlah']*$r['harga']);
                  }
                   ?>
                    <tfoot>
                      <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Total :</th>
                        <th>Rp.<?php echo number_format($total); ?></th>
                        <th></th>
                      </tr>
                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
<?php
include "footer.php";
?>