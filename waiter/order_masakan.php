<?php
include "header.php";
?>
<?php
include "../koneksi.php";

$edit = mysqli_query($conn, "SELECT * FROM detail_order INNER JOIN masakan ON detail_order.id_masakan = masakan.id_masakan where id_order='$_GET[id_order]'");
$r=mysqli_fetch_array($edit);
?>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <a href="order.php" class="btn btn-danger">Kembali</a>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                   <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Keterangan</th>
                        <th>Gambar</th>
                        <th>Harga</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
<?php
include "../koneksi.php";
$no=1;
$data = "SELECT * from masakan where status_masakan='Y'";
$bacadata = mysqli_query($conn, $data);
while($r = mysqli_fetch_array($bacadata))
{
?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $r['nama_masakan']; ?></td>
                        <td><?php echo $r['desk_masakan']; ?></td>
                        <td><img src='<?php echo "../admin/images/".$r['gambar']; ?>' height='80'></td>
                        <td>Rp.<?php echo number_format($r['harga']); ?></td>
                        <td><a href="#" class="btn btn-info" data-toggle="modal" data-target="#myModal<?php echo $r['id_masakan'];?>"><i class="glyphicon glyphicon-plus"></i></a></td>
                      </tr>
            <div class="modal" id="myModal<?php echo $r['id_masakan'];?>" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Order Makanan</h4>
                  </div>
                  <div class="modal-body">
          <?php
            $id = $r['id_masakan']; 
            $id_order = $_GET['id_order']; 
            $query_edit = mysqli_query($conn,"SELECT * FROM masakan WHERE id_masakan='$id'");
            $query_order = mysqli_query($conn,"SELECT * FROM pesan_meja WHERE id_order='$id_order'");
            $r = mysqli_fetch_array($query_edit);
            $data = mysqli_fetch_array($query_order);
          ?>
                    <form role="form"  method="POST" action="proses_tambah_order.php?id_masakan=<?php echo $id;?>" enctype="multipart/form-data" class="form-horizontal form-material">
                        <div class="box-body">
                             <input type="hidden" class="form-control" name="id_order" value="<?php echo $data['id_order']; ?>">
                             <input type="hidden" class="form-control" name="keterangan" value="Y">
                              <input type="hidden" class="form-control" name="id_masakan" value="<?php echo $r['id_masakan'];?>">
                            <div class="form-group form-float">
                              <div class="form-line">
                                <input type="text" class="form-control" name="jumlah" required>
                                <label class="form-label">Jumlah Masakan</label>
                              </div>
                            </div>
                            <div class="form-group form-float">
                              <div class="form-line">
                                <textarea type="text" class="form-control" name="keterangan_detail"></textarea>
                                <label class="form-label">Keterangan</label>
                              </div>
                            </div>
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Pesan</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
                    <?php }  ?>
                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php
include "../koneksi.php";

$data = mysqli_query($conn, "SELECT * FROM detail_order INNER JOIN masakan ON detail_order.id_masakan = masakan.id_masakan where id_order='$_GET[id_order]'");
$r=mysqli_fetch_array($data);
?>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                   <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Pesanan</th>
                        <th>Keterangan</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Total</th>
                      </tr>
                    </thead>
<?php
include "../koneksi.php";
  $no = 1;
  $total = 0;
  $query = mysqli_query($conn, "SELECT * FROM detail_order INNER JOIN masakan 
    ON detail_order.id_masakan = masakan.id_masakan where id_order='$_GET[id_order]' 
    GROUP BY masakan.id_masakan");
    while( $data = mysqli_fetch_array($query)){
  $jumlah_barang = mysqli_num_rows($query);
  $jumlah_harga = $data['harga'] * $data['jumlah'];
  $total += $jumlah_harga;
?>
                    <tbody>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $data['nama_masakan'];?></td>
                        <td><?php echo $data['keterangan_detail'];?></td>
                        <td>Rp.<?php echo number_format($data['harga']);?></td>
                        <td>
                          <a href="#" class="btn btn-default"><i class="glyphicon glyphicon-plus" aria-hidden="true"></i></a>
                          <?php echo $data['jumlah']; ?>
                          <a href="#" class="btn btn-default"><i class="glyphicon glyphicon-minus" aria-hidden="true"></i></a>
                        </td>
                        <td>Rp.<?php echo number_format($jumlah_harga); ?></td>
                      </tr>
                    </tbody>
<?php } if($total == 0){ ?>
                        <td colspan="4" align="center"><?php echo "Tabel Makanan Anda Kosong!"; ?></td>
<?php } else { ?>
                        <td colspan="8" style="font-size: 18px;"><b><div class="pull-right">Total Harga Anda : Rp. <?php echo number_format($total); ?>,- </div> </b></td>
<?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
<?php
include "footer.php";
?>