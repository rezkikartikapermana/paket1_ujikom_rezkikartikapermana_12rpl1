<?php
include "header.php";
?>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h4>
                               Data Transaksi
                            </h4>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                      <tr>
                        <th>No</th>
                        <th>id Order</th>
                        <th>No Meja</th>
                        <th>Nama Pelanggan</th>
                        <th>Tanggal</th>
                        <th>Detail Order</th>
                      </tr>
                    </thead>
                    <tbody>
<?php
include "../koneksi.php";
$no=1;
$data = "SELECT * from detail_order INNER JOIN pesan_meja ON detail_order.id_order = pesan_meja.id_order INNER JOIN meja ON pesan_meja.no_meja = meja.no_meja INNER JOIN masakan ON detail_order.id_masakan = masakan.id_masakan where status_order='N' GROUP BY detail_order.id_order";
$bacadata = mysqli_query($conn, $data);
while($r = mysqli_fetch_array($bacadata))
{
    $jumlah_harga = $r['harga'] * $r['jumlah'];
?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $r['id_order']; ?></td>
                        <td><?php echo $r['no_meja']; ?></td>
                        <td><?php echo $r['nama_pelanggan']; ?></td>
                        <td><?php echo $r['tanggal']; ?></td>
                        <td><a href="detail_transaksi.php?table=detail_order&id_order=<?php echo $r['id_order'];?>" class="btn btn-success">Detail</a></td>
                      </tr>
<?php } ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
            
<?php
include "footer.php";
?>