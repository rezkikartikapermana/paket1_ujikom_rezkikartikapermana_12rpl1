<?php  
include "header.php";
?>
	<!-- Cart -->
	<section class="cart bgwhite p-t-100 p-b-100">
		<div class="container">
			<!-- Cart item -->
			<div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                       <thead>
                      <tr>
                        <th></th>
                        <th>Nama Pesanan</th>
                        <th>Harga</th>
                        <th>Keterangan</th>
                        <th>Jumlah</th>
                        <th>Total</th>
                        <th>Batalkan</th>
                      </tr>
                    </thead>
<?php 
include "../koneksi.php";
  $id_order=$_SESSION['id_order'];
  $query_mysqli = mysqli_query($conn, "SELECT * FROM cart INNER JOIN masakan ON cart.id_masakan = masakan.id_masakan where id_order='$_SESSION[id_order]'")or die(mysqli_error());
      while($data = mysqli_fetch_array($query_mysqli))
      {
      	$jumlah_harga = $data['harga'] * $data['jumlah'];

?>
                    <tbody>
                      <tr>
                        <td>
                        	<div class="cart-img-product b-rad-4 o-f-hidden">
								<img src="<?php echo "../admin/images/".$data['gambar']; ?>" alt="IMG-PRODUCT">
							</div>
                        </td>
                        <td><?php echo $data['nama_masakan']; ?></td>
                        <td>Rp.<?php echo $data['harga']; ?></td>
                        <td><textarea name="keterangan[]" ><?php echo $data['ket']; ?></textarea></td>
                        <td>
                        	<div class="flex-w bo5 of-hidden w-size17">
									<button class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">
										<i class="fs-12 fa fa-minus" aria-hidden="true"></i>
									</button>

									<input class="size8 m-text18 t-center num-product" type="number" name="jumlah" value="<?php echo $data['jumlah']; ?>">

									<button class="btn-num-product-up color1 flex-c-m size7 bg8 eff2">
										<i class="fs-12 fa fa-plus" aria-hidden="true"></i>
									</button>
								</div>
                        </td>
                        <td>Rp.<?php echo number_format($data['jumlah']*$data['harga']);?></td>
                        <td><a href="cancel_orderan.php?id_cart=<?php echo $data['id_cart'];?>&id_order=<?php echo $data['id_order'];?>" class="btn btn-danger btn-md">
                                <i class="material-icons">delete</i>
                            </a>
                        </td>
                      </tr>
                    </tbody>
                    <?php 
                  }
                   ?>
                                </table>
                            </div>
                        </div>
			<div class="flex-w flex-sb-m p-t-25 p-b-25 bo8 p-l-35 p-r-60 p-lr-15-sm">
				<div class="flex-w flex-m w-full-sm">
					<div class="size11 bo4 m-r-10">
						<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="coupon-code" placeholder="Coupon Code">
					</div>

					<div class="size12 trans-0-4 m-t-10 m-b-10 m-r-10">
						<!-- Button -->
						<button class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
							Apply coupon
						</button>
					</div>
				</div>
			</div>
<?php 
include "../koneksi.php";
  $id_order=$_SESSION['id_order'];
  $query_mysqli = mysqli_query($conn, "SELECT * FROM cart INNER JOIN masakan ON cart.id_masakan = masakan.id_masakan where id_order='$_SESSION[id_order]'")or die(mysqli_error());
  $data = mysqli_fetch_array($query_mysqli);
      	$jumlah_harga = $data['harga'] * $data['jumlah'];

?>
			<!-- Total -->
			<div class="bo9 w-size18 p-l-40 p-r-40 p-t-30 p-b-38 m-t-30 m-r-0 m-l-auto p-lr-15-sm">
				<h5 class="m-text20 p-b-24">
					Cart Totals
				</h5>

				<!--  -->
				<div class="flex-w flex-sb-m p-b-12">
					<span class="s-text18 w-size19 w-full-sm">
						Subtotal:
					</span>

					<span class="m-text21 w-size20 w-full-sm">
						$39.00
					</span>
				</div>

				<!--  -->
				<div class="flex-w flex-sb-m p-t-26 p-b-30">
					<span class="m-text22 w-size19 w-full-sm">
						Total:
					</span>

					<span class="m-text21 w-size20 w-full-sm">
						$39.00
					</span>
				</div>

				<div class="size15 trans-0-4">
					<!-- Button -->
					<a href="checkout.php?id_order=<?php echo $data['id_order']; ?>">
						<button class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
							Pesan Sekarang
						</button>
					</a>
				</div>
			</div>
		</div>
	</section>
	<?php  
include "footer.php";
?>