<?php  
include "header.php";
?>

	<!-- Slide1 -->
	<section class="slide1">
		<div class="wrap-slick1">
			<div class="slick1">
				<div class="item-slick1 item1-slick1" style="background-image: url(../assets/images/utama.jpg);">
					<div class="wrap-content-slide1 sizefull flex-col-c-m p-l-15 p-r-15 p-t-150 p-b-170">
						<h2 class="caption1-slide1 xl-text2 t-center bo14 p-b-6 animated visible-false m-b-22" data-appear="fadeInUp">
							BapeResto
						</h2>

						<span class="caption2-slide1 m-text1 t-center animated visible-false m-b-33" data-appear="fadeInDown">
							Silahkan Buka Menu Help Bila Tidak Memahami Fitur Yg Kami Berikan
						</span>

						<div class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="zoomIn">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- content page -->
	<section class="bgwhite p-t-66 p-b-38">
		<div class="container">
			<div class="row">
				<div class="col-md-4 p-b-30">
					<div class="hov-img-zoom">
						<img src="../assets/images/ab.jpg" height="300" width="60" alt="IMG-ABOUT">
					</div>
				</div>

				<div class="col-md-8 p-b-30">
					<h3 class="m-text26 p-t-15 p-b-16">
						Tentang kami BapeResto
					</h3>

					<p class="p-b-28">
						BapeResto adalah restoran kreasi anak muda yg merupakan kumpulan menu dengan tema bucin atau baper, pelanggan tak hanya di buat bingung dengan kata-kata manis tetapi dibuat kenyang dan ketagihan dengan menu yg disediakan, sehingga timbul rasa rindu untuk kembali mencicipi menu yg kami sediakan..
						ayo buat dirimu baper dengan menu-menu istimewa kami!!
					</p>

					<div class="bo13 p-l-29 m-l-9 p-b-10">
						<p class="p-b-11">
							Creativity is just connecting things. When you ask creative people how they did something, they feel a little guilty because they didn't really do it, they just saw something. It seemed obvious to them after a while.
						</p>

						<span class="s-text7">
							- Steve Job’s
						</span>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- New Product -->
	<section class="newproduct bgwhite p-t-45 p-b-105">
		<div class="container">
			<div class="sec-title p-b-60">
				<h3 class="m-text5 t-center">
					Menu Populer
				</h3>
			</div>

			<!-- Slide2 -->
			<div class="wrap-slick2">
				<div class="slick2">
<?php
include "../koneksi.php";
$data = "SELECT * from masakan where status_masakan='Y'";
$bacadata = mysqli_query($conn, $data);
while($r = mysqli_fetch_array($bacadata))
{
?>
					<div class="item-slick2 p-l-15 p-r-15">
						<!-- Block2 -->
						<div class="block2">
							<div class="block2-img wrap-pic-w of-hidden pos-relative block2">
								<img src="<?php echo "../admin/images/".$r['gambar']; ?>" height="300" alt="IMG-PRODUCT">

								<div class="block2-overlay trans-0-4">
									<a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
										<i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
										<i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
									</a>
								</div>
							</div>

							<div class="block2-txt p-t-20">
								<h6><b><span class="block2-name dis-block s-text3 p-b-5">
									<?php echo $r['nama_masakan']; ?>
								</span></b></h6>
								<span class="block2-price m-text6 p-r-5">
									<b>Rp.<?php echo $r['harga']; ?></b>
								</span></br>
								<p><?php echo $r['desk_masakan']; ?></p>
							</div>
						</div>
					</div>
<?php } ?>
				</div>
			</div>

		</div>
	</section>
<?php  
include "footer.php";
?>