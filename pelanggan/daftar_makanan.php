<?php  
include "header.php";
?>      
        	<!-- Banner -->
        	<div class="banner bgwhite p-t-40 p-b-40">
        		<div class="container">
        			<div class="row">
<?php
include "../koneksi.php";
$data = "SELECT * from masakan where jenis='Makanan' and status_masakan='Y'";
$bacadata = mysqli_query($conn, $data);
while($r = mysqli_fetch_array($bacadata))
{
?>
        				<div class="col-sm-12 col-md-6 col-lg-3 p-b-50">
                  
        					<!-- block1 -->
        					<div class="block1 hov-img-zoom pos-relative m-b-30">
        						<img src="<?php echo "../admin/images/".$r['gambar']; ?>" height="300" alt="IMG-BENNER">
        						<div class="block1-wrapbtn w-size2">
        							<!-- Button -->
        							<a href="#" class="flex-c-m size2 m-text2 bg3 hov1 trans-0-4" data-toggle="modal" data-target="#myModal<?php echo $r['id_masakan'];?>"> Pesan </a>
        						</div>
        					</div>

        					<div class="block2-txt p-t-20">
        						<h6><b><span class="block2-name dis-block s-text3 p-b-5">
        							<?php echo $r['nama_masakan']; ?>
        						</span></b></h6>

        						<span class="block2-price m-text6 p-r-5">
        							<b>Rp.<?php echo $r['harga']; ?></b>
        						</span></br>

        						<p><?php echo $r['desk_masakan']; ?></p>
        					</div>
        				</div>

<div class="modal" id="myModal<?php echo $r['id_masakan'];?>" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Order Makanan</h4>
                  </div>
                  <div class="modal-body">
          <?php
            $id = $r['id_masakan'];
            $id_order=$_SESSION['id_order'];
            $query_mysqli = mysqli_query($conn, "SELECT * FROM pesan_meja where id_order='$_SESSION[id_order]'");
            $query_edit = mysqli_query($conn,"SELECT * FROM masakan WHERE id_masakan='$id'");
            $data = mysqli_fetch_array($query_mysqli);
            $r = mysqli_fetch_array($query_edit);
          ?>
                    <form role="form"  method="POST" action="proses_cart.php?id_masakan=<?php echo $id;?>" enctype="multipart/form-data" class="form-horizontal form-material">
                        <div class="box-body">
                             <input type="hidden" class="form-control" name="id_order" value="<?php echo $data['id_order']; ?>">
                              <input type="hidden" class="form-control" name="id_masakan" value="<?php echo $r['id_masakan'];?>">
                            <div class="form-group form-float">
                              <div class="form-line">
                                <label class="form-label">Jumlah Masakan</label>
                                <div class="bo4 of-hidden size15 m-b-20">
                                  <input class="sizefull s-text7 p-l-22 p-r-22" type="number" name="jumlah" placeholder="Jumlah">
                                </div>
                              </div>
                            </div>
                            <div class="form-group form-float">
                              <div class="form-line">
                                <label class="form-label">Keterangan</label>
                                <textarea class="dis-block s-text7 size20 bo4 p-l-22 p-r-22 p-t-13 m-b-20" name="ket" placeholder="Apa ada pesan khusus ?"></textarea>
                              </div>
                            </div>
                      </div><!-- /.box-body -->
                      <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Pesan</button>
                  </div><!-- /.box-body -->
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
<?php }  ?>
			</div>
		</div>
	</div>

	<?php  
include "footer.php";
?>