<?php
include "header.php";
?>
<!-- Basic Examples -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    <a href="#masakantambah" class="btn bg-pink waves-effect" data-toggle="modal">Tambah Masakan</a>
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                         <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Keterangan</th>
                            <th>Jenis</th>
                            <th>Gambar</th>
                            <th>Harga</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
<?php
include "../koneksi.php";
$no=1;
$data = "SELECT * from masakan";
$bacadata = mysqli_query($conn, $data);
while($r = mysqli_fetch_array($bacadata))
{
$gambar              = "images/".$r['gambar'];
?>
                          <tr>
                            <td><?php echo $no++; ?></td> 
                            <td><?php echo $r['nama_masakan']; ?></td>
                            <td><?php echo $r['desk_masakan']; ?></td>
                            <td><?php echo $r['jenis']; ?></td>
                            <td><img src='<?php echo $gambar ?>' height='100'></td>
                            <td><?php echo $r['harga']; ?></td>
                            <td>
                                <a href="#" class="btn btn-warning btn-md" data-toggle="modal" data-target="#makananedit<?php echo $r['id_masakan'];?>">Edit </a>
<?php
   if($r['status_masakan'] == 'Y') {
?>
                                <a href="status_masakan.php?table=masakan&id_masakan=<?php echo $r['id_masakan'];;?>&action=not-verifed" class="btn btn-info btn-md"> Tersedia </a>
<?php } else { ?>
                                <a href="status_masakan.php?table=masakan&id_masakan=<?php echo $r['id_masakan'];?>&action=verifed" class="btn btn-danger btn-md"> Habis </a>
<?php } ?>
                            </td>
                          </tr>
 <div class="modal fade" id="makananedit<?php echo $r['id_masakan'];?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Edit Masakan</h4>
            </div>
<?php
$id_masakan = $r['id_masakan'];
$query_edit = mysqli_query($conn,"SELECT * FROM masakan WHERE id_masakan='$id_masakan'");
$r = mysqli_fetch_array($query_edit);
?>
<div class="modal-body">
    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="body">
                <form id="form_validation" action="proses_edit_masakan.php?id_masakan=<?php echo $id_masakan;?>" enctype="multipart/form-data" class="form-horizontal form-material" method="POST">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" value="<?php echo $r['id_masakan'];?>" disabled required>
                            <label class="form-label">Id User</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="nama_masakan" value="<?php echo $r['nama_masakan'];?>" required>
                            <label class="form-label">Nama Masakan</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="harga" value="<?php echo $r['harga'];?>" placeholder="Masukan Harga" required>
                            <label class="form-label">Harga</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="file" name="gambar" value="<?php echo $r['gambar'];?>">
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <textarea class="form-control" name="desk_masakan" rows="3" required><?php echo $r['desk_masakan'];?></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SAVE CHANGES</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
             <!-- #END# Basic Validation -->
            </div>
        </div>
    </div>
</div>
                    </tbody>
                    <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
            <!-- #END# Basic Examples -->
<div class="modal fade" id="masakantambah" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Tambah Masakan</h4>
            </div>
            <div class="modal-body">
                <!-- Basic Validation -->
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="body">
                            <form id="form_validation" action="proses_tambah_masakan.php" method="POST" enctype="multipart/form-data" class="form-horizontal form-material">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="nama_masakan" required>
                            <label class="form-label">Nama Masakan</label>
                        </div>
                    </div></br>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input name="jenis" type="radio" value="Makanan" class="with-gap" id="radio_3" checked />
                                <label for="radio_3">Makanan</label>
                            <input name="jenis" type="radio" value="Minuman" id="radio_4" class="with-gap" />
                                <label for="radio_4">Minuman</label>
                            <input name="jenis" type="radio" value="Paketan" id="radio_5" class="with-gap" />
                                <label for="radio_5">Paketan</label>
                        </div>
                    </div></br>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="harga" required>
                            <label class="form-label">Harga Masakan</label>
                        </div>
                    </div></br>
                    <div class="form-group form-float">
                        <div class="form-line">
                          <input type="file" name="gambar">
                        </div>
                    </div></br>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <textarea class="form-control" name="desk_masakan" rows="3" required></textarea>
                            <label class="form-label">Keterangan</label>
                        </div>
                    </div></br>
                <input type="hidden" class="form-control" value="N" name="status_masakan">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">TAMBAH MASAKAN</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
             <!-- #END# Basic Validation -->
            </div>
        </div>
    </div>
</div>
            
<?php
include "footer.php";
?>