<?php
include'database.php';
$db = new database();
?>
<?php
include "header.php";
?>
<!-- Basic Examples -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    <a href="#mejatambah" class="btn bg-pink waves-effect" data-toggle="modal">Tambah Meja</a>
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                          <tr>
                            <th>No Meja</th>
                            <th>Gambar</th>
                            <th>Aksi</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
<?php
include "../koneksi.php";
$data = "SELECT * from meja";
$bacadata = mysqli_query($conn, $data);
while($select_result = mysqli_fetch_array($bacadata))
{
$no_meja          = $select_result['no_meja'];
$gambar              = "images/".$select_result['gambar'];
$status              = $select_result['status_meja'];

echo"<tr>
<td>$no_meja</td>
<td><img src='$gambar' height='100'></td>
";
?>
                            <td>
                                <a href="#" class="btn btn-warning btn-md" data-toggle="modal" data-target="#mejaedit<?php echo $no_meja;?>">Edit</a>
                            </td>
                            <td>
            <?php
               if($status == 'Y') {
            ?>
                                <a href="status_meja.php?table=meja&no_meja=<?php echo $no_meja;?>&action=not-verifed" class="btn btn-info btn-md"> Digunakan </a>
            <?php
                } else {
            ?>
                                <a href="status_meja.php?table=meja&no_meja=<?php echo $no_meja;?>&action=verifed" class="btn btn-danger btn-md"> Belum Digunakan </a>
            <?php } ?>
                            </td>
                        </tr>
 <div class="modal fade" id="mejaedit<?php echo $no_meja;?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Edit Meja</h4>
            </div>
<?php
$query_edit = mysqli_query($conn,"SELECT * FROM meja WHERE no_meja='$no_meja'");
$r = mysqli_fetch_array($query_edit);
?>
            <div class="modal-body">
                <!-- Basic Validation -->
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="body">
                            <form id="form_validation" action="proses_edit_meja.php?no_meja=<?php echo $no_meja;?>" enctype="multipart/form-data" class="form-horizontal form-material" method="POST">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="no_meja" value="<?php echo $r['no_meja'];?>" required>
                                        <label class="form-label">No Meja</label>
                                    </div>
                                </div><br>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="file" name="gambar" value="<?php echo $r['gambar'];?>">
                                    </div>
                                </div>
                                <input type="hidden" class="form-control" value="<?php echo $r['status_meja'];?>" name="status_meja">
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-link waves-effect">SAVE CHANGES</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
 <!-- #END# Basic Validation -->
            </div>
        </div>
    </div>
</div>
                    </tbody>
            <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Examples -->
<div class="modal fade" id="mejatambah" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Tambah Meja</h4>
            </div>
                <div class="modal-body">
                    <!-- Basic Validation -->
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="body">
                                <form id="form_validation" action="proses_tambah_meja.php" method="POST" enctype="multipart/form-data" class="form-horizontal form-material">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="no_meja" required>
                                            <label class="form-label">No Meja</label>
                                        </div>
                                    </div></br>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="file" name="gambar">
                                        </div>
                                    </div>
                                    <input type="hidden" class="form-control" value="N" id="exampleInputPassword1" name="status_meja">
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-link waves-effect">TAMBAH MEJA </button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
 <!-- #END# Basic Validation -->
            </div>
        </div>
    </div>
</div>

<?php
include "footer.php";
?>