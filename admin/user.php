<?php
include'database.php';
$db = new database();
?>
<?php
include "header.php";
?>
<!-- Basic Examples -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    <a href="#usertambah" class="btn bg-pink waves-effect" data-toggle="modal">Tambah User</a>
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Email</th>
                            <th>Level</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
<?php
  $no = 1;
  foreach($db->tampil_data() as $x){
?>
                          <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $x['nama_user']; ?></td>
                            <td><?php echo $x['username']; ?></td>
                            <td><?php echo $x['password']; ?></td>
                            <td><?php echo $x['email']; ?></td>
                            <td><?php echo $x['nama_level']; ?></td>
                            <td><?php
                                  if($x['status'] == 'Y'){
                                ?>
                            <a href="approve.php?table=user&id_user=<?php echo $x['id_user']; ?>&action=not-verifed" class="btn btn-success btn-md"> Aktif </a>
                                <?php
                                  }else{
                                ?>
                            <a href="approve.php?table=user&id_user=<?php echo $x['id_user']; ?>&action=verifed" class="btn btn-danger btn-md"> NonAktif </a>
                                <?php } ?>
                            </td>
                            <td>
                                <a href="#" class="btn btn-warning btn-md" data-toggle="modal" data-target="#useredit<?php echo $x['id_user'];?>">Edit</a>
                            </td>
                          </tr>
 <div class="modal fade" id="useredit<?php echo $x['id_user'];?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Edit User</h4>
            </div>
<?php
$id = $x['id_user']; 
$query_edit = mysqli_query($conn,"SELECT * FROM user WHERE id_user='$id'");
$r = mysqli_fetch_array($query_edit);
?>
<div class="modal-body">
    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="body">
                    <form id="form_validation" action="proses.php?id_user=<?php echo $r['id_user'];?>&aksi=update" enctype="multipart/form-data" class="form-horizontal form-material" method="POST">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="id_user" value="<?php echo $r['id_user'];?>" disabled required>
                                <label class="form-label">Id User</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="nama_user" value="<?php echo $r['nama_user'];?>" required>
                                <label class="form-label">Nama</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="username" value="<?php echo $r['username'];?>" required>
                                <label class="form-label">Username</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control" name="password" value="<?php echo $r['password'];?>" required>
                                <label class="form-label">Password</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="email" class="form-control" name="email" value="<?php echo $r['email'];?>" required>
                                <label class="form-label">Email</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <?php 
                               include "../koneksi.php";
                                    $data = "SELECT * from Level";
                                    $bacadata = mysqli_query($conn, $data);
                                    while($r = mysqli_fetch_array($bacadata))
                                    {
                                        $id_level = $r['id_level'];
                                        $nama_level = $r['nama_level'];
                                 ?>
                                <input name="id_level" value="<?php echo $id_level;?>" type="radio" class="with-gap" id="radio_1<?php echo $r['id_level'] ?>" checked />
                                    <label for="radio_1<?php echo $r['id_level'] ?>"><?php echo $nama_level;?></label>
                                    <?php } ?>
                            </div>
                        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-link waves-effect">SAVE CHANGES</button>
            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
        </div>
                    </form>
                </div>
            </div>
        </div>
 <!-- #END# Basic Validation -->
            </div>
        </div>
    </div>
</div>
                        </tbody>
<?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Examples -->
<div class="modal fade" id="usertambah" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Tambah User</h4>
            </div>
            <div class="modal-body">
                <!-- Basic Validation -->
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="body">
                                <form id="form_validation" action="proses.php?aksi=tambah" method="POST">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="nama_user" required>
                                            <label class="form-label">Nama</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                            <label class="form-label">Username</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="password" class="form-control" name="password" required>
                                            <label class="form-label">Password</label>
                                        </div>
                                    </div>
                                    <input type="hidden" class="form-control" value="N" id="exampleInputPassword1" name="status">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="email" class="form-control" name="email" required>
                                            <label class="form-label">Email</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <?php 
                                           include "../koneksi.php";
                                                $data = "SELECT * from Level";
                                                $bacadata = mysqli_query($conn, $data);
                                                while($r = mysqli_fetch_array($bacadata))
                                                {
                                                    $id_level = $r['id_level'];
                                                    $nama_level = $r['nama_level'];
                                             ?>
                                            <input name="id_level" value="<?php echo $id_level;?>" type="radio" class="with-gap" id="radio<?php echo $r['id_level'] ?>" />
                                                <label for="radio<?php echo $r['id_level'] ?>"><?php echo $nama_level;?></label>
                                                <?php } ?>
                                        </div>
                                    </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-link waves-effect">TAMBAH USER</button>
            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
        </div>
                                </form>
                            </div>
                        </div>
                    </div>
             <!-- #END# Basic Validation -->
            </div>
        </div>
    </div>
</div>
            
<?php
include "footer.php";
?>