<?php
include 'header.php';
?>
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                LAPORAN TRANSAKSI RESTORAN
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Pelanggan</th>
                                            <th>No Meja</th>
                                            <th>Tanggal</th>
                                            <th>Kasir</th>
                                            <th>Total</th>
                                            <th>Detail Order</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
include "../koneksi.php";
$no=1;
$data = "SELECT * from transaksi INNER JOIN pesan_meja ON transaksi.id_order = pesan_meja.id_order INNER JOIN user ON transaksi.id_user = user.id_user where status_order='Y' GROUP BY transaksi.id_order";
$bacadata = mysqli_query($conn, $data);
while($r = mysqli_fetch_array($bacadata))
{
?>
                      <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $r['nama_pelanggan']; ?></td>
                        <td><?php echo $r['no_meja']; ?></td>
                        <td><?php echo $r['tanggal']; ?></td>
                        <td><?php echo $r['nama_user']; ?></td>
                        <td><?php echo $r['total_bayar']; ?></td>
                        <td><a href="detail_transaksi.php?table=detail_order&id_order=<?php echo $r['id_order'];?>" class="btn btn-success">Detail</a></td>
                      </tr>
<?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
<?php
include 'footer.php';
?>