<?php
include "header.php";
?>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body">
                <div class="row clearfix">
        <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
            <div class="panel-group" id="accordion_19" role="tablist" aria-multiselectable="true">
                <div class="panel panel-col-pink">
                    <div class="panel-heading" role="tab" id="headingOne_19">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapseOne_19" aria-expanded="true" aria-controls="collapseOne_19">
                                <i class="material-icons">perm_contact_calendar</i> Detail Masakan
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne_19" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_19">
                        <div class="panel-body">
                            <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                  <tr>
                                    <th>No</th>
                                    <th>Nama Pesanan</th>
                                    <th>Keterangan</th>
                                    <th>Jumlah</th>
                                    <th>Harga</th>
                                    <th>Total</th>
                                  </tr>
                                </thead>
<?php
include "../koneksi.php";
$no=1;
$total=0;
$data = "SELECT * from detail_order INNER JOIN masakan ON detail_order.id_masakan = masakan.id_masakan  where id_order='$_GET[id_order]'";
$bacadata = mysqli_query($conn, $data);
while($r= mysqli_fetch_array($bacadata))
{
    $jumlah_harga    = $r['harga'] * $r['jumlah'];
    $total          += ($r['jumlah']*$r['harga']);
    $id_order      = $r['id_order'];
?>
                                <tbody>
                                  <tr>
                                    <td><?php echo $no++ ;?></td>
                                    <td><?php echo $r['nama_masakan'];?></td>
                                    <td><?php echo $r['keterangan_detail'];?></td>
                                    <td><?php echo $r['jumlah'];?></td>
                                    <td>Rp.<?php echo number_format($r['harga']);?></td>
                                    <td>Rp.<?php echo number_format($jumlah_harga)?></td>
                                  </tr>
                                </tbody>
<?php } ?>
                                <tfoot>
                                  <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>Total :</th>
                                    <th>Rp.<?php echo number_format($total); ?></th>
                                  </tr>
                                </tfoot>
                                </table>
                            </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Multiple Items To Be Open -->

                        </div>
                    </div>
                </div>
            </div>
<?php
include "footer.php";
?>